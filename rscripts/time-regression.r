
nevals = "1000000"
lr = 0.01
t  = 0

options(scipen=999, width=10000)
columns = c("time.s", "core.size", "lp.sol", "best.known", "best.found", "gap", "gap%", "nevals")

cnames = c("Instance", "Best", rep(c("$Sw_{d=1}$", "$Sw_{d=2}$", "$Sh_{d=1}$", "$Sh_{d=2}$", "$CBGA$"),2))

x = c(100,250,500)


plot(x, x, ylim=c(0,100))
splot  = function(dir, d, RGop, DCases) 
{
    prefix = paste(dir, d, sep='')
    for (n in x)
    {
        for (idx in 0:29) {
            idx = sprintf("%02d", idx)

            instance = paste(prefix, n, idx, sep="-")

            for (RG_operator in RGop)
            {
                for (DC in DCases) 
                {
                    instance_file = paste(instance, RG_operator, DC, 0, t, lr, nevals, sep="-")

                    exp = read.table(paste(instance_file, ".out", sep=''))
                    print(instance_file)
                    time = exp[, 1]  # Average best and time
                    points(n, mean(time))

                }
            }
        }
    }
}

cat("Example:\twrite_results(dir, instance_name, INST, RGopMut, DCases)\n");
cat("Example:\twrite_results('output/', '10-500', 0:10, c(0,1), c(1,2))\n");

instance_names = c('5-100', '10-100',  '5-250', '10-250',  '5-500', '10-500', '30-100')






